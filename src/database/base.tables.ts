import { PrimaryGeneratedColumn, CreateDateColumn, BaseEntity} from "typeorm"

export class Basetable extends BaseEntity{

    @PrimaryGeneratedColumn('increment')
    id: number;

    @CreateDateColumn({ nullable:true})
    createdAt: Date;
}


import { modeView } from "src/utils/entity.enum";
import { Basetable } from "../../database/base.tables";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Users } from "./users.entity";
import { Likes } from "./likes.entity";
import { Comments } from "./comments.entity";

@Entity('posts')
export class Posts extends Basetable {
  
    @Column({ 
        type: 'varchar', 
        length:7, 
        nullable:true,
        default: modeView.PUBLIC
    })
    visibility: string;

    @CreateDateColumn({ nullable: true})
    postedOn: Date;

    @Column({ type: 'varchar'})
    text: string;
    
    @ManyToOne(() => Users, user => user.posts)
    @JoinColumn({ name: 'userId'})
    user: Users;

    @Column()
    userId: number

    @OneToMany(() => Likes, postsLikes => postsLikes.posts, {cascade: true})
    postsLikes?: Likes[] | null;

    @OneToMany(() => Comments, postsComments=> postsComments.posts, {cascade: true})
    postsComments?: Comments[] | null;
}
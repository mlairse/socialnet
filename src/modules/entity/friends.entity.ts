import { Basetable } from "../../database/base.tables";
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Users } from "./users.entity";

@Entity('friends')
export class Friends extends Basetable {
  
    @ManyToOne(() => Users, userOne => userOne.usersFriends)
    @JoinColumn({ name: 'userOneId'})
    userOne: Users;

    @Column()
    userOneId: string

    @ManyToOne(() => Users, friend => friend.id)
    @JoinColumn({ name: 'friendId'})
    friend: Users;

    @Column()
    friendId: string
}
import { modeView } from "src/utils/entity.enum";
import { Basetable } from "../../database/base.tables";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Users } from "./users.entity";
import { Posts } from "./posts.entity";

@Entity('likes')
export class Likes extends Basetable {
  
    @CreateDateColumn({ nullable: true})
    likedOn: Date;
    
    @Column({
        type: 'boolean', 
        default: true, 
        nullable: false
    })
    didLike: boolean;

    @ManyToOne(() => Users, user => user.usersLikes)
    @JoinColumn({ name: 'userId'})
    user: Users;

    @Column()
    userId: number

    @ManyToOne(() => Posts, posts => posts.postsLikes)
    @JoinColumn({ name: 'postsId'})
    posts: Posts;

    @Column()
    postsId: number
    
}
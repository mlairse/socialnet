import { Post } from "@nestjs/common";
import { Basetable } from "../../database/base.tables";
import { Column, Entity, OneToMany } from 'typeorm';
import { Posts } from "./posts.entity";
import { Followers } from "./followers.entity";
import { Friends } from "./friends.entity";
import { Likes } from "./likes.entity";
import { Comments } from "./comments.entity";

@Entity('users')
export class Users extends Basetable {
  
    @Column({ type: 'varchar', nullable: false })
    fullName: string;

    @OneToMany(() => Posts, posts => posts.user)
    posts?: Posts[] | null

    @OneToMany(() => Followers, usersFollowers => usersFollowers.userOne, {cascade: true})
    usersFollowers?: Followers[] | null;

    @OneToMany(() => Friends, usersFriends => usersFriends.userOne, {cascade: true})
    usersFriends?: Friends[] | null;

    @OneToMany(() => Likes, usersLikes => usersLikes.user, {cascade: true})
    usersLikes?: Likes[] | null;

    @OneToMany(() => Comments, usersComments => usersComments.user, {cascade: true})
    usersComments?: Comments[] | null;

}
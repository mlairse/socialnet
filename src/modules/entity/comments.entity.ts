import { modeView } from "src/utils/entity.enum";
import { Basetable } from "../../database/base.tables";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Users } from "./users.entity";
import { Posts } from "./posts.entity";

@Entity('comments')
export class Comments extends Basetable {
    
    @Column({
        type: 'varchar', 
        nullable: false
    })
    text: string;

    @ManyToOne(() => Users, user => user.usersComments)
    @JoinColumn({ name: 'userId'})
    user: Users;

    @Column()
    userId: number

    @ManyToOne(() => Posts, posts => posts.postsComments)
    @JoinColumn({ name: 'postsId'})
    posts: Posts;

    @Column()
    postsId: number
    
}
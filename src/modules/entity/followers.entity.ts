import { modeView } from "src/utils/entity.enum";
import { Basetable } from "../../database/base.tables";
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Users } from "./users.entity";

@Entity('followers')
export class Followers extends Basetable {
  
    @ManyToOne(() => Users, userOne => userOne.usersFollowers)
    @JoinColumn({ name: 'userOneId'})
    userOne: Users;

    @Column()
    userOneId: string

    @ManyToOne(() => Users, follower => follower.id)
    @JoinColumn({ name: 'followerId'})
    follower: Users;

    @Column()
    followerId: string
}
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Posts } from './entity/posts.entity';
import { Users } from './entity/users.entity';
import { PostsController } from './controller/posts.controller';
import { UsersController } from './controller/users.controller';
import { PostsRepository } from './repositories/posts.repository';
import { UsersRepository } from './repositories/users.repository';
import { UsersService } from './services/users.service';
import { PostService } from './services/posts.service';
import { FollowersRepository } from './repositories/followers.repository';
import { FriendsRepository } from './repositories/friends.repository';
import { LikesRepository } from './repositories/likes.repository';
import { Followers } from './entity/followers.entity';
import { Friends } from './entity/friends.entity';
import { Likes } from './entity/likes.entity';
import { WallsController } from './controller/wall.controller';
import { WallService } from './services/wall.service';

@Module({
  imports: [TypeOrmModule.forFeature([
    Posts,
    Users,
    Followers,
    Friends,
    Likes,
  ])],

  controllers: [
    UsersController,
    PostsController,
    WallsController,
  ],

  providers: [
    // ...Services
    UsersService,
    PostService, 
    WallService,
    // ...Repositories
    UsersRepository,
    PostsRepository,
    FollowersRepository,
    FriendsRepository,
    LikesRepository,
  ],

  exports: [
    UsersService,
    PostService, 
    WallService,
  ]
})
export class LocalModule {}

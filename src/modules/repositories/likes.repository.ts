import { Repository } from 'typeorm';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Likes } from '../entity/likes.entity';

@Injectable()
export class LikesRepository {

  constructor(
    @InjectRepository(Likes)
    private readonly _Repository: Repository<Likes>,
  ) {

  }
  getRepository(){
    return this._Repository;
  }
}
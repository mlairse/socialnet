import { Repository } from 'typeorm';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Friends } from '../entity/friends.entity';

@Injectable()
export class FriendsRepository {

  constructor(
    @InjectRepository(Friends)
    private readonly _Repository: Repository<Friends>,
  ) {

  }
  getRepository(){
    return this._Repository;
  }
}
import { Repository } from 'typeorm';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Followers } from '../entity/followers.entity';

@Injectable()
export class FollowersRepository {

  constructor(
    @InjectRepository(Followers)
    private readonly _Repository: Repository<Followers>,
  ) {

  }
  getRepository(){
    return this._Repository;
  }
}
import { Repository } from 'typeorm';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Users } from '../entity/users.entity';

@Injectable()
export class UsersRepository {

  constructor(
    @InjectRepository(Users)
    private readonly _Repository: Repository<Users>,
  ) {

  }
  getRepository(){
    return this._Repository;
  }

  getWall(userId: number){
    const trust = true
    return this._Repository.createQueryBuilder('users')
        .select()
        .leftJoinAndSelect('users.posts', 'posts')
        .leftJoinAndSelect('posts.postsLikes', 'postsLikes')
        .leftJoinAndSelect('users.usersFriends', 'usersFriends')
        .leftJoinAndSelect('users.usersFollowers', 'usersFollowers')
        .where('users.id = :userId', {userId})
        .orderBy('posts.postedOn', 'DESC')
        .getOne()
  }
}
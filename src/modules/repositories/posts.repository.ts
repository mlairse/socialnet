import { Repository } from 'typeorm';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Posts } from '../entity/posts.entity';

@Injectable()
export class PostsRepository {

  constructor(
    @InjectRepository(Posts)
    private readonly _Repository: Repository<Posts>,
  ) {

  }
  getRepository(){
    return this._Repository;
  }
}
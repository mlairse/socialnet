
import { Exclude, Expose } from "class-transformer";
import { Followers } from "src/modules/entity/followers.entity";
import { Friends } from "src/modules/entity/friends.entity";
import { Posts } from "src/modules/entity/posts.entity";
import { OutputPostsDto } from "./posts.output.dto";

@Exclude()
export class OutputWallDto {
  @Expose()
  fullname: string;

  @Expose()
  UserFriends?: Friends[] | null;

  @Expose()
  totalFriends: number

  @Expose()
  UserFollowers?: Followers[] | null;

  @Expose()
  totalFollowers: number

  @Expose()
  PostsWall?: OutputPostsDto[] | null;
}


import { Exclude, Expose } from "class-transformer";

@Exclude()
export class OutputPostsDto {

  @Expose()
  text: string;

  @Expose()
  postedOn: Date;

  @Expose()
  visibility: string;

  @Expose()
  meLike: boolean;

  @Expose()
  totalLike: number;
}


import { ApiProperty } from "@nestjs/swagger";
import { Exclude, Expose } from "class-transformer";
import { IsNotEmpty } from "class-validator";

@Exclude()
export class InputLikeDto {

  @ApiProperty()
  @Expose()
  @IsNotEmpty()
  postId: number;

  @ApiProperty()
  @Expose()
  @IsNotEmpty()
  userId: number;

  @ApiProperty()
  @Expose()
  @IsNotEmpty()
  didLike: boolean;

}

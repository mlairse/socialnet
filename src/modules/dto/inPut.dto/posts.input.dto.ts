
import { ApiProperty } from "@nestjs/swagger";
import { Exclude, Expose } from "class-transformer";
import { IsNotEmpty, IsOptional } from "class-validator";

@Exclude()
export class InputPostsDto {

  @ApiProperty()
  @Expose()
  @IsNotEmpty()
  text: string;

  @ApiProperty()
  @Expose()
  @IsOptional()
  visibility?: string | null;

  @ApiProperty()
  @Expose()
  @IsNotEmpty()
  userId: number;
}

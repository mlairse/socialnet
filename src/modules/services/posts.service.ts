import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { PostsRepository } from '../repositories/posts.repository';
import { Posts } from '../entity/posts.entity';
import { InputPostsDto } from '../dto/inPut.dto/posts.input.dto';
import { modeView } from 'src/utils/entity.enum';
import { InputLikeDto } from '../dto/inPut.dto/like.input.dto';
import { Likes } from '../entity/likes.entity';
import { LikesRepository } from '../repositories/likes.repository';

const realtionsPosts = [
  'user',
  'postsLikes',
  'postsComments',
]
@Injectable()
export class PostService {
    constructor(
        private readonly _postsRepository: PostsRepository,
        private readonly _likeRepository: LikesRepository,
      ) { }
    
      async getAll(): Promise <Posts[]>{
        const list: Posts[] = await this._postsRepository.getRepository().find({
          relations: realtionsPosts
        })
        
        return list;
      }

      async create( input: InputPostsDto ): Promise<Posts> {    
        const newEl = new Posts();   
        newEl.text = input.text
        newEl.userId = input.userId
        if (input.visibility === modeView.PRIVATE){
          newEl.visibility = input.visibility
        }
    
        try {
          const saved = await this._postsRepository.getRepository().save(newEl);
          return saved;
        } catch (error) {
          throw new BadRequestException(error);
        }
      }   

      async beLike( input: InputLikeDto ): Promise<Likes> {    
        const isExist = await this._likeRepository.getRepository().findOne({
          where:{ 
            userId: input.userId,
            postsId: input.postId,
          }
        })
        let element: Likes
        if(!isExist){
          element = new Likes();   
          element.postsId = input.postId
          element.userId = input.userId
        } else {
          if (!input.didLike){
            isExist.didLike = false
            element = isExist
          } else {
            isExist.didLike = input.didLike
            element = isExist
          }
        }      
    
        try {
          const saved = await this._likeRepository.getRepository().save(element);
          return saved;
        } catch (error) {
          throw new BadRequestException(error);
        }
      } 

}


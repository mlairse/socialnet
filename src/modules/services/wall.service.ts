import { Injectable } from '@nestjs/common';
import { UsersRepository } from '../repositories/users.repository';
import { OutputWallDto } from '../dto/outPut.dto/wall.output.dto';
import { OutputPostsDto } from '../dto/outPut.dto/posts.output.dto';
import { LikesRepository } from '../repositories/likes.repository';


@Injectable()
export class WallService {
    constructor(
        private readonly _userRepository: UsersRepository,
        private readonly _likeRepository: LikesRepository,
        
      ) { }

      async getWall(userId: number): Promise <OutputWallDto>{
        const userData = await this._userRepository.getWall(userId) 
        let arrayPosts: OutputPostsDto[] = []
        let medidLike = false
        
        if (userData.posts.length > 0){
          for (let i=0; i < userData.posts.length; i++){
            const myLike = await this._likeRepository.getRepository().findOne({
              where:{
                userId: userId,
                postsId: userData.posts[i].id
              }
            })

            if (myLike) {
              medidLike = myLike.didLike
            }
            const data: OutputPostsDto ={
              text: userData.posts[i].text,
              postedOn: userData.posts[i].postedOn,
              visibility: userData.posts[i].visibility,
              meLike: medidLike,
              totalLike: userData.posts[i]?.postsLikes?.filter(like => like.didLike).length
            }
            arrayPosts.push(data)
            medidLike = false
          }
        }
          
        const wall: OutputWallDto = {
          fullname: userData.fullName,
          UserFriends: userData.usersFriends,
          totalFriends: userData.usersFriends?.length,
          UserFollowers: userData.usersFollowers,
          totalFollowers: userData.usersFollowers?.length,
          PostsWall: arrayPosts
        }    
        return wall;
      } 
      
}


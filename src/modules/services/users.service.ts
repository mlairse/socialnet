import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { UsersRepository } from '../repositories/users.repository';
import { Users } from '../entity/users.entity';
import { Friends } from '../entity/friends.entity';
import { FriendsRepository } from '../repositories/friends.repository';
import { FollowersRepository } from '../repositories/followers.repository';
import { Followers } from '../entity/followers.entity';

const realtionsUsers =[
  'usersFriends',
  'usersFollowers'
]
@Injectable()
export class UsersService {
    constructor(
        private readonly _userRepository: UsersRepository,
        private readonly _friendRepository: FriendsRepository,
        private readonly _followerRepository: FollowersRepository,
      ) { }

      async getAll(): Promise <Users[]>{
        const list = await this._userRepository.getRepository().find({
          relations: realtionsUsers
        })        
        return list;
      } 
    
      async create( name: string ): Promise<Users> {    
        const newEl = new Users();   
        newEl.fullName = name;
    
        try {
          const saved = await this._userRepository.getRepository().save(newEl);
          return saved;
        } catch (error) {
          throw new BadRequestException(error);
        }
      }   
      
      async beFriend(userId: number, friendId: number): Promise <Friends[]>{
        const user = await this._userRepository.getRepository().findOne({
          where: { id: userId }
        })
        if (!user) throw new NotFoundException('The userId don`t exist');
        const userFriend = await this._userRepository.getRepository().findOne({
          where: { id: friendId }
        })
        if (!userFriend) throw new NotFoundException('The friendId don`t exist');

        let relations = await this._friendRepository.getRepository().findOne({
          where: {
            userOneId: userId,
            friendId: friendId
          }
        })
        if (!relations) {
          relations = new Friends()
          relations.friend = userFriend
          relations.userOne = user
        } else {
          throw new NotFoundException('The relationship as friend is already exist');
        }        
        
        try {
          const saved = await this._friendRepository.getRepository().save(relations);
          const result = await this._friendRepository.getRepository().find({
            where: { userOneId: user.id }
          })
          return result;
        } catch (error) {
          throw new BadRequestException(error);
        }
      } 
      
      async beFollower(userId: number, user2Id: number): Promise <Followers[]>{
        const user = await this._userRepository.getRepository().findOne({
          where: { id: userId }
        })
        if (!user) throw new NotFoundException('The userId don`t exist');
        const userFollower = await this._userRepository.getRepository().findOne({
          where: { id: user2Id }
        })
        if (!userFollower) throw new NotFoundException('The followerId don`t exist');

        let relations = await this._followerRepository.getRepository().findOne({
          where: {
            userOneId: userId,
            followerId: user2Id
          }
        })
        if (!relations) {
          relations = new Followers()
          relations.follower = userFollower
          relations.userOne = user
        } else {
          throw new NotFoundException('The relationship as follower is already exist');
        }        
        
        try {
          const saved = await this._followerRepository.getRepository().save(relations);
          const result = await this._followerRepository.getRepository().find({
            where: { userOneId: user.id }
          })
          return result;
        } catch (error) {
          throw new BadRequestException(error);
        }
      }
      
}


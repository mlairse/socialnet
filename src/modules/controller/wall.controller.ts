import { Controller, Get, Post, Param, Body } from '@nestjs/common';
import { OutputWallDto } from '../dto/outPut.dto/wall.output.dto';
import { WallService } from '../services/wall.service';

@Controller('walls')
export class WallsController {
    constructor(
        private readonly _wallService: WallService,
    ) { }

    @Get(':userId')
    async getByID(@Param('userId') userId: number): Promise <OutputWallDto> {
        return await this._wallService.getWall(userId)
    }

}
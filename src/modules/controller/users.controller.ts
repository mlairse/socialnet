import { Controller, Get, Post, Param } from '@nestjs/common';
import { Users } from '../entity/users.entity';
import { UsersService } from '../services/users.service';
import { Friends } from '../entity/friends.entity';
import { Followers } from '../entity/followers.entity';

@Controller('users')
export class UsersController {
    constructor(private readonly _userService: UsersService) { }
  
    @Get()
    getAll(): Promise <Users[]> {
        return this._userService.getAll();
    }
    
    @Post(':fullName')
    create(@Param('fullName') fullName: string): Promise<Users>{
        return this._userService.create(fullName);
    }

    @Post(':user1Id/friends/:user2Id')
    beFriends(@Param('user1Id') user1Id: number, @Param('user2Id') user2Id: number,): Promise<Friends[]>{
        return this._userService.beFriend(user1Id, user2Id);
    }

    @Post(':user1Id/followers/:user2Id')
    beFollowers(@Param('user1Id') user1Id: number, @Param('user2Id') user2Id: number,): Promise<Followers[]>{
        return this._userService.beFollower(user1Id, user2Id);
    }

}


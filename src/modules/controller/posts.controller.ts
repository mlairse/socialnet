import { Controller, Get, Post, Body } from '@nestjs/common';
import { Posts } from '../entity/posts.entity';
import { PostService } from '../services/posts.service';
import { InputPostsDto } from '../dto/inPut.dto/posts.input.dto';
import { InputLikeDto } from '../dto/inPut.dto/like.input.dto';
import { Likes } from '../entity/likes.entity';

@Controller('posts')
export class PostsController {
    constructor(private readonly _postService: PostService) { }

    @Get()
    getAll(): Promise <Posts[]> {
        return this._postService.getAll();
    }

    @Post()
    create(@Body() input: InputPostsDto): Promise<Posts>{
        return this._postService.create(input);
    }

    @Post('/like')
    beLike(@Body() input: InputLikeDto): Promise<Likes>{
        return this._postService.beLike(input);
    }

}


## Social Network
[[_TOC_]]

### Configure and check .env
-  In the .env file you can configure the necessary global project variables, including the database name.

## Access to the Project in GitLab (public)
- (https://gitlab.com/mlairse/socialnet.git)

## Installation
```bash
$ npm install
```

# How to setup postgres server in development with docker
```bash
$ docker run --name postgres_socialnet -e POSTGRES_PASSWORD=test -e POSTGRES_USER=test -e POSTGRES_DB=socialnet -p 5455:5432 -d postgres
```

## TypeORM commands
```bash
# for generate new migration
$ npm run migration:generate:win "nameMigration"

# it necessary run pending migrations 
$ npm run migration:run:win
```

## Running the app
```bash
# development
$ npm run start

# watch mode 
$ npm run start:dev

# production mode
$ npm run start:prod
```
## How can you enter the Swagger's project
- [Swagger.Access](https://localhost:8080/api/docs)


## Other test descriptions 
- Read the document Word attachment in the project for other decriptions for test.
    [Document guide for test.docx]


## Team Members
- Team Leader, Backend Developer - [Misleidy Lairse Lujan Herrera](https://www.gitlab.com/mlairse)

## Stay in touch
- [https://www.linkedin.com/in/mlairse](email:mlairse@gmail.com)
